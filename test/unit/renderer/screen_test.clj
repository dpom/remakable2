(ns unit.renderer.screen-test
  (:require
    [clojure.test :refer :all]
    [mirror.renderer.screen :as tut]))


(def events
  [{:pen 1, :x 17884, :y 2923} ; 0
   {:x 17885}
   {:x 17886, :y 2922}
   {:y 2850}
   {:touch 1, :x 18057, :pres 620}
   {:pres 825}
   {:x 18058, :y 2508, :pres 1069}
   {:touch 0, :pres 0}
   {:pen 0}
   {:pen 1, :x 17568, :y 4482}
   {:touch 1, :x 18038, :pres 1142} ; 10
   {:x 17799, :y 3283, :pres 1330}
   {:touch 0, :pres 0}
   {:x 18164, :y 4893}
   {:pen 0}
   {:rubber 1, :x 18028, :y 3265}
   {:x 18482, :y 3676, :pres 479}
   {:touch 1, :x 18038, :pres 1142}
   {:x 18047, :y 3291, :pres 954}
   {:x 18450, :y 3630}
   {:touch 0, :pres 0} ; 20
   {:rubber 0}
   {:pen 1, :x 17558, :y 2861}
   {:touch 1, :pres 756}
   {:x 18192, :y 2832, :pres 2428}
   {:touch 0, :pres 0}
   {:pen 0}])


(deftest new-state-test
  (is (= (reduce tut/new-state tut/init-state events)
         {:x 18192, :y 2832, :pres 0,
          :action :out,
          :strokes
          [[[18057 2850 620] [18057 2850 825] [18058 2508 1069]] [[18038 4482 1142] [17799 3283 1330]]
           [[17558 2861 756] [18192 2832 2428]]],
          :cur-stroke [],
          :erasures
          [[[18038 3676 1142] [18047 3291 954] [18450 3630 954]]],
          :cur-erasure []})))
